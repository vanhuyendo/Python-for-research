# Data camp
#In this case study, we will find and plot the distribution of word frequencies for each translation of Hamlet. 
#Perhaps the distribution of word frequencies of Hamlet depends on the translation --- let's find out!

#For these exercises, functions count_words_fast, read_book, 
#and word_stats are already defined as in the Case 2 Videos (Videos 3.2.x).

#word_count_distribution(text) that takes a book string and returns a 
#dictionary with items corresponding to the count of times a collection of words appears in the translation, 
#and values corresponding to the number of number of words that appear with that frequency.

#Write a function word_count_distribution(text) that takes a book string and 
#returns a dictionary with items corresponding to the count of times a collection of words appears in the translation, 
a#nd values corresponding to the number of number of words that appear with that frequency.

text="a function word_count_distribution(text) that takes a book string and returns a dictionary with items corresponding to the count of times a collection of words appears in the translation, and values corresponding to the number of number of words that appear with that frequency."
"


def word_count_distribution1(text):
    count_words=count_fast(text)
    count_distribution={}
    for word in count_words:
        if count_words[word] in count_distribution:
            count_distribution[ count_words[word] ]+=1
        else:
            count_distribution[ count_words[word] ]=1
    return count_distribution


#use counter function in collections
from collections import Counter

def word_count_distribution(text):
    count_words=count_fast(text)
    count_distribution=Counter(count_words.values())
    return count_distribution

distribution=word_count_distribution1(text)

#????? dictionary is automatically sorted?

#    Create a function more_frequent(distribution) that takes a word frequency dictionary 
#(like that made in Exercise 1) and outputs a dictionary with the same keys as those in distribution 
#(the number of times a group of words appears in the text), and values corresponding to the fraction 
#of words that occur with more frequency than that key.
 #   Call more_frequent(distribution), and store as cumulative.
 
 def more_frequent(distribution):
     cumulative_frequent={}
     for frequency in distribution:
         cumulative_frequent[frequency]=sum(distribution[f] for f in distribution if f>frequency )
     num_frequencies=sum(distribution.values())
     for f in cumulative_frequent:
         cumulative_frequent[f]=cumulative_frequent[f]/num_frequencies
     return cumulative_frequent
   
    
 text="The key-function patterns shown above are Python Python very very very common, so Python provides convenience functions to make accessor functions easier and faster"   
distribution=word_count_distribution1(text)   
    
more_frequent(distribution)

#using cumsum from numpy
import numpy as np

d=distribution

keys, values = zip(*sorted(d.items(), key=itemgetter(1)))
total = sum(values)


#
Hint

    You might begin with storing the counts of the distribution as follows: counts = list(distribution.keys())
    Store the values of the distribution with frequency_of_counts = list(distribution.values()). 
    numpy is preloaded into memory as np: find the cumulative sum of these using np.cumsum().
    To obtain the fraction of words more frequent than this, divide this cumulative sum by its maximum, 
    and subtract this value from 1. You're ready to make a dictionary with the results of this calculation as values and counts as keys!



##

d=distribution
def stupid_function(d):
    counts=sorted(list(d.keys()))
    f=[d[i] for i in counts]
    fraction=np.cumsum(f)
    fraction=[1-fraction[i]/fraction[-1] for i in range(len(fraction))]
    more_f={}
    for i in range(len(fraction)):        
        more_f[counts[i]]=fraction[i]
    return more_f

stupid_function(distribution)

#### stupid function by stupid data camp
def more_frequent(d):
    counts=list(d.keys())
    f=list(d.values())
    fraction=np.cumsum(f)
    fraction=[1-fraction[i]/fraction[-1] for i in range(len(fraction))]
    more_f={}
    for i in range(len(fraction)):        
        more_f[counts[i]]=fraction[i]
    return more_f


cumulative=more_frequent(distribution)

##############################################

Edit the code used to read though each of the books in our library, and store the word frequency distribution for each translation of William Shakespeare's "Hamlet" as a Pandas dataframe hamlets with columns named "language" and "distribution". word_count_distribution is preloaded from Exercise 1. How many translations are there? Which languages are they translated into?

hamlets = pd.DataFrame(columns=("language", "distribution"))
book_dir = "Books"
title_num = 1
for language in book_titles:
    for author in book_titles[language]:
        for title in book_titles[language][author]:
            if title == "Hamlet":
                inputfile = data_filepath+"Books/"+language+"/"+author+"/"+title+".txt"
                text = read_book(inputfile)
                distribution = word_count_distribution(text)
                hamlets.loc[title_num] = language, distribution
                title_num += 1
#########################################
colors = ["crimson", "forestgreen", "blueviolet"]
handles, hamlet_languages = [], []
for index in range(hamlets.shape[0]):
    language, distribution = hamlets.language[index+1], hamlets.distribution[index+1]
    dist = more_frequent(distribution)
    plot, = plt.loglog(sorted(list(dist.keys())),sorted(list(dist.values()),
        reverse = True), color = colors[index], linewidth = 2)
    handles.append(plot)
    hamlet_languages.append(language)
plt.title("Word Frequencies in Hamlet Translations")
xlim    = [0, 2e3]
xlabel  = "Frequency of Word $W$"
ylabel  = "Fraction of Words\nWith Greater Frequency than $W$"
plt.xlim(xlim); plt.xlabel(xlabel); plt.ylabel(ylabel)
plt.legend(handles, hamlet_languages, loc = "upper right", numpoints = 1)
# show your plot using `plt.show`!