# introduction to Pandas
# series
import pandas as pd

x=pd.Series(["1",2,3], index=["a","b","c"])
x


# data frame

data={"names": ["b","a","c"], "age": [12,13,12]}

x=pd.DataFrame(data, columns=["names","age"], index=["9","3","6"])
x
x["names"][0]
x.names[0]

ind=sorted(x.index, key=)

x.reindex(index=ind)

#loading and inspecting data
import os

os.getcwd()
os.chdir('./course/python_for_research_edx/case_study_whisky')

import numpy as np

whiskies = pd.read_csv("whiskies.txt")
whiskies["regions"]=pd.read_csv("regions.txt")
whiskies.head()
whiskies.iloc[0:10]
whiskies.iloc[0:5,0:5]
whiskies.columns
flavors=whiskies.iloc[:,2:13]

cor_flavor=pd.DataFrame.corr(flavors)

#correlation
import matplotlib.pyplot as plt

plt.figure(figsize=(10,10))
plt.pcolor(cor_flavor)
plt.colorbar()


cor_whiskies=pd.DataFrame.corr(flavors.transpose())
plt.figure(figsize=(10,10))
plt.pcolor(cor_whiskies)
plt.axis("tight")

#clustering
from sklearn.cluster.bicluster import SpectralCoclustering

model=SpectralCoclustering(n_clusters=6, random_state=0)

model.fit(cor_whiskies)
model.rows_

np.sum(model.rows_, axis=1)
np.sum(model.rows_, axis=0)
model.row_labels_

#
whiskies["group"]=pd.Series(model.row_labels_, index=whiskies.index)

whiskies=whiskies.ix[np.argsort(model.row_labels_)]
whiskies=whiskies.reset_index(drop=True)

corr_clusters=pd.DataFrame.corr(whiskies.iloc[:,2:13].transpose())

plt.figure(figsize=(10,10))
plt.pcolor(corr_clusters)
plt.colorbar()