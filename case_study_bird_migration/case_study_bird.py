import os

os.getcwd()
os.chdir("./course/python_for_research_edx/case_study_bird_migration")

import pandas as pd
bird_tracking=pd.read_csv("bird_tracking.csv")
bird_tracking.info()
bird_tracking.head()


#ploting trajectories of birds

import matplotlib.pyplot as plt
import numpy as np

ind=bird_tracking.bird_name=="Eric"

plt.plot(bird_tracking.latitude[ind],bird_tracking.longitude[ind])

bird_names=pd.unique(bird_tracking.bird_name)
bird_names

plt.figure(figsize=(10,10))
for name in bird_names:
    ind=bird_tracking.bird_name==name
    plt.plot(bird_tracking.latitude[ind],bird_tracking.longitude[ind], ".", label=name)
plt.xlabel("Longititude")
plt.ylabel("Latitude")
plt.legend(loc = "right upper")
plt.savefig("trajectories.pdf")

## look at speed

Eric=bird_tracking.bird_name=="Eric"

speed=bird_tracking.speed_2d[Eric]

plt.hist(speed)
plt.hist(speed[:10])

ind=np.isnan(speed)
ind.any()
sum(ind)

plt.hist(speed[~ind],bins=np.linspace(0,30,20),normed=True)

plt.xlabel("speed m/s")
plt.ylabel("frequencies")


speed.plot(kind="hist", range=(0,30))

####

bird_tracking.columns

import datetime

datetime.datetime.today()
datetime.datetime.strptime(bird_tracking.date_time.iloc[1][:-3], "%Y-%m-%d %H:%M:%S")



time_eclapsed=[]
for k in range(0, len(bird_tracking)):
    time_eclapsed.append(datetime.datetime.strptime\
            (bird_tracking.date_time.iloc[k][:-3], "%Y-%m-%d %H:%M:%S"))

bird_tracking["time_eclapsed"]=pd.Series(time_eclapsed, index=bird_tracking.index)
    
bird_tracking["time_eclapsed_1"]=time_eclapsed
    
bird_tracking.head()    

times=bird_tracking.time_eclapsed[bird_tracking.bird_name=="Eric"]

times_elapsed=[time-times[0] for time in times]    
times_elapsed[0]    
times_elapsed[1000]/datetime.timedelta(days=1)
time_elapsed=np.array(times_elapsed)/datetime.timedelta(days=1)

plt.plot(time_elapsed)
len(times_elapsed)

####

times_elapsed[:5]
bird_tracking.head()

data=bird_tracking[bird_tracking.bird_name=="Eric"]

next_day=1
index=[]
mean_speed=[]
for (i,t) in enumerate(time_elapsed):
    if t < next_day:
        index.append(i)
    else:
        mean_speed.append(np.mean(data.speed_2d[index]))
        index=[]
        next_day+=1
        
len(mean_speed)

plt.plot(mean_speed)

### using cartopy

import cartopy.crs as ccrs
import cartopy.feature as cfeature

proj=ccrs.Mercator()

names= pd.unique(bird_tracking.bird_name)

bird_tracking.columns

plt.figure(figsize=(10,10))
ax=plt.axes(projection=proj)
ax.set_extent((-25.0, 20.0, 52.0,10.0))

ax.add_feature(cfeature.LAND)
ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.COASTLINE)
ax.add_feature(cfeature.BORDERS, linestyle=":")

for name in names:
    indx= bird_tracking.bird_name==name
    xx,yy=bird_tracking.latitude[indx], bird_tracking.longitude[indx]
    ax.plot(yy,xx, ".", label=name ,transform=ccrs.Geodetic())
    
plt.xlabel("Longitude")
plt.ylabel("Latitude")
plt.legend(loc="right upper")
plt.savefig("maps.pdf")
    














