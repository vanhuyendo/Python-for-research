#### implement the kNN method.

import numpy as np


# Problem: classify a given point to a group.

# Procedure for kNN method
    # 1. Calculate distances between the given point and all sample points.
    #2. Find the k nearest points to the given point.
    #3. Find the number of points among the k nearest points of each group.
    #4. Find the most frequent group.
    #5. Assign the given point to the most frequent group.


#1. 
def distance(p1,p2):
    """
    Find the L2-distance between point p1 and point p2.
    
    input: p1, p2: two numpy arrays 
    output: a positive float 
    """
    return np.sqrt(sum(np.square(p1-p2)))

p1=np.array([1,1])

p2=np.array([2,3])

distance(p1,p2)


#2. 
def k_points(p, sample, k=5):
    """
    Find k nearest points from set of points sample to point p. 
    
    input:  - sample: array N*K where N is the number of sample points, K dimension point
            - p: array dimension K
            - k: the number of nearest points.
    ouput:  - list of tuble with position and corresponding distance of k-nearest points. 
                If there are many points apart same distance to the point p, we take all those points.
    """
    distances={}
    for i in range(len(sample)):
        distances[i]=distance(sample[i], p)  
    distances_sorted=sorted(distances.items(), key=lambda x:x[1])
    distances_s=[distances_sorted[i] for i in range(len(distances)) if distances_sorted[i][1]<=distances_sorted[k-1][1]]    
    return distances_s

sample=np.array([[1,1],[2,2],[2,2],[1,1],[2,2],[2,2],[3,3]])

p=np.array([0,0])

dis=k_points(p, sample,k=3)
dis

k=2
#3
def group_k_points(k_distances, group):
    """
    Identify points to theirs respective group.
    
    input: - k_distances: list of k nearest points including positions and distances apart from the detected point.
            - group: labels of points.
    output: - group: list of labels of k nearest points.
    """
    group_k=[group[k_distances[i][0]] for i in range(len(k_distances))]
    return group_k
    
group={1: "red", 2: "blue", 3: "blue", 4: "red", 5: "red", 6: "blue", 0: "blue"}

gr=group_k_points(dis,group)    


#4
from collections import Counter
def frequencies(group_nearest):
    """
    Find frequencies of each group.
    
    input:  - group_nearest: a list of labels
            - output: dictionary of frequencies of labels.
    """
    f=Counter(group_nearest)
    return dict(f)
    
freq=frequencies(gr)

#5
def most_frequencies(freq):
    """
    Find the most frequency. Return random one among same most frequencies.
    
    input:  - freq: list of frequencies of labels.
    output: - the label with highest frequency.
    """
    freq_sorted=sorted(freq.items(), key=lambda x:x[1])
    most_f=np.random.choice([freq_sorted[i][0] for i in range(len(freq)) if freq_sorted[i][1]==freq_sorted[-1][1]])
    return most_f

most_frequencies(freq)


#6

def kNN(p,sample,k,group):
    """
    Classify a point by kNN method.
    
    input:  - p: array of the point to detect
            - sample: array of all points 
            - k: the number of points for preference.
            - group: labels of the sample.
    output: - the group: the label of the most frequency among k nearest points from p.
    """
    k_distances=k_points(p, sample, k)
    k_group=group_k_points(k_distances, group)
    k_freq=frequencies(k_group)
    the_group=most_frequencies(k_freq)
    return the_group

kNN(p,sample,7,group)
    

##############
#1. Majority vote function

import random

def majority_vote(votes):
    '''
    Look for the most frequent element of a list, a random element returned if many labels have the same frequency.
    
    input:  - votes: list of labels.
    output: - the most frequent label.
    '''    # votes: list of votes
    vote_counts={}
    for vote in votes:
        if vote in vote_counts:
            vote_counts[vote]+=1
        else:
            vote_counts[vote]=1
    max_counts=max(vote_counts.values())
    winners=[]
    for vote, count in vote_counts.items():
        if count==max_counts:
            winners.append(vote)
    return random.choice(winners) 
    
import scipy.stats as ss

def majority_vote_short(votes):
    '''
    An alternative function to find the most frequent element.
    '''
    mode, counts=ss.mstats.mode(votes)
    return mode
    

votes=[1,1,2,2,3,3,3,3,3,2,2,1,1]
majority_vote_short(votes)



#2.

points=np.array([[1,1],[1,2],[1,3],[2,1],[2,2],[2,3],[3,1],[3,2],[3,3]])

p=np.array([2,2.5])

import matplotlib.pyplot as plt

plt.plot(p[0],p[1], "ro");
plt.plot(points[:,0],points[:,1],"bo");
plt.axis([0.5,3.5,0.5,3.5])


def find_nearest_neighbor_points(p,points,k=5):
    '''
    Find k nearest points among training points to the target point.
    
    input:  - p: reference point, an array dimention (D,)
            - points: set of training points, array dimention (N,D)
            - k: the number of nearest points to select, defaut is 5
    output: - list of index of k nearest points.
    '''
    distances=np.zeros(points.shape[0])
    for i in range(len(points)):
        distances[i]=distance(p, points[i])
    ind=np.argsort(distances)
    return ind[:k]

def knn_predict(p,points,outcomes,k):
    '''
    Predict the label of a given point by kNN
    
    input:  - p: reference point, an array dimention (D,)
            - points: set of training points, array dimention (N,D)
            - k: the number of nearest points to select
            - outcomes: labels of points, list length D
    output: - label of point p.
    '''
    ind=find_nearest_neighbor_points(p,points,k)
    return majority_vote(outcomes[ind])


#generating data

def generate_points(n=50):
    """Generate points.
    """
    points=np.concatenate((ss.norm(0,1).rvs((n,2)),ss.norm(1,1).rvs((n,2))),axis=0)
    outcomes=np.concatenate((np.repeat(0,n),np.repeat(1,n)),axis=0)
    return (points, outcomes)

n=20
points, outcomes=generate_points(n)

plt.figure()
plt.plot(points[:n,0], points[:n,1], "ro")
plt.plot(points[n:,0], points[n:,1], "bo")
plt.axis((-2.5,3,-3,3))


def knn_prediction_grid(points, outcomes,k,h,x_min,x_max,y_min,y_max):
    '''
    Prediction of grid points.
    
    input:  - points: training points, array of 2 dimension points, dimension (N,2)
            - outcomes: labels of training points, list of length N
            - x_min, x_max, y_min, limit of coordinates.
            - h, k: interval space.
    output: - List of predicted labels of grid points.
    '''
    xs=np.arange(x_min, x_max, h)
    ys=np.arange(y_min, y_max, k)
    xx, yy =np.meshgrid(xs,ys)
    predictions=np.zeros(xx.shape, dtype=int)
    for i,x in enumerate(xs):
        for j,y in enumerate(ys):
            p=np.array([x,y])
            predictions[j,i]=knn_predict(p,points, outcomes,k)
    return (xx,yy,predictions)
            



(x_min,x_max,y_min,y_max)=(-2.5,3,-3,3)
h=0.1
k=5


(xx,yy,predictions)=knn_prediction_grid(points, outcomes,k,h,x_min,x_max,y_min,y_max)

#

plt.figure()
#plt.plot(points[:n,0], points[:n,1], "ro")
#plt.plot(points[n:,0], points[n:,1], "bo")
plt.axis((-2.5,3,-3,3))
background_colormap = ListedColormap (["hotpink","lightskyblue", "yellowgreen"])
plt.pcolormesh(xx, yy, predictions,cmap = background_colormap, alpha = 0.3)

plt.scatter(points[:,0], points [:,1], c = outcomes, cmap = observation_colormap, s = 30)