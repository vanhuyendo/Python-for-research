##A cipher is a secret code for a language. 
#In this case study, we will explore a cipher that is reported by contemporary Greek historians t
#to have been used by Julius Caesar to send secret messages to generals during times of war.
#The Caesar cipher shifts each letter of this message to another letter in the alphabet, which is a fixed number of letters away from the original. If our encryption key were 1, we would shift h to the next letter i, i to the next letter j, and so on. If we reach the end of the alphabet, which for us is the space character, we simply loop back to a. To decode the message, we make a similar shift, 
#except we move the same number of steps backwards in the alphabet.

# Let's look at the lowercase letters.
import string
string.ascii_lowercase

# We will consider the alphabet to be these letters, along with a space.
alphabet = string.ascii_lowercase + " "

# create `letters` here!
letters={}
for i in range(len(alphabet)):
    letters[i]=alphabet[i] 


#optimal code by data camp
alphabet = string.ascii_lowercase + " "
letters = dict(enumerate(alphabet))

encryption_key = 3

# define `encoding` here!
encoding={}
for i in range(len(alphabet)):
    encoding[alphabet[i]]=(i+encryption_key)%27

encoding

#
message = "hi my name is caesar"

def encode(encryption_key):
    encoding={}
    for i in range(len(alphabet)):
        encoding[alphabet[i]]=(i+encryption_key)%27
    return encoding

def caesar(message, encryption_key):
    # return the encoded message as a single string!
    convert=""
    encoding=encode(encryption_key)
    for s in message:
        convert+=letters[encoding[s]]
    return convert

caesar(message, 3)

decoded_message=caesar(encoded_message, -3)
print(decoded_message)