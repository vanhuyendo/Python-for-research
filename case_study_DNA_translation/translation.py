mRNA=open("mRNA.txt", "r")
mRNA=mRNA.read()
mRNA=mRNA.replace("\n", "")


##### read text file

def read_text(input_file):
    """Reads and returns a text file."""
    file=open(input_file, "r")
    file=file.read()
    file=file.replace("\n","")
    return file

mRNA=read_text("mRNA.txt")
protein=read_text("protein.txt")

table = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
}


###########? adding string loop
def translation(DNA):
    if len(DNA)%3==0:
        start_points=range(0,len(DNA),3)
        protein=""
        for i in start_points:
            protein+=table[DNA[i:(i+3)]]       
        return protein
    else:
        print("Make sure the length of DNA is divisible by 3." )
        
DNA=mRNA[20:26]
translation(DNA)
translation(mRNA[20:938])[:-1] == translation(mRNA[20:935])
